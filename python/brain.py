import json
import sys
import track as trck
import carpos
from math import log, exp

class CarState(object):

    def __init__(self, name):
        self.vel = 0
        self.angVel = 0
        self.acc = 0
        self.angAcc = 0
        self.crashed = False
        # Default CarPos
        self.carPos = carpos.CarPos(name)
        
    def reset(self):
        self.vel = 0
        self.angVel = 0
        self.acc = 0
        self.angAcc = 0
        
    def __str__(self):
        return "Pos: " + str(self.carPos.pieceIndex()) \
            + " - " + str(self.carPos.inPieceDistance()) \
            + " @ " + str(self.carPos.angle()) \
            + " vel: " + str(self.vel) \
            + " acc: " + str(self.acc) \
            + " aVel: " + str(self.angVel) \
            + " aAcc: " + str(self.angAcc) 
        
    def update(self, newCarPos, track, gameTick):
        """
        carPos is the fragment from carPositions msg
        """
        if newCarPos.name() != self.carPos.name():
            raise Exception(
                "Updating incoherent info: expected name %s, got %s"
                % (self.carPos.name(), newCarPos.name())
                )
                
        if gameTick < 0:
            # Cant compute vel or acc with only the first position
            self.vel = 0
            self.angVel = 0
            self.acc = 0
            self.angAcc = 0
            self.carPos = newCarPos
        else:
            # Compute vel..
            try:
                new_vel = track.distBetweenPositions(self.carPos, newCarPos)
            except trck.ErrorComputeLaneSwitchLen:
                # if vel is not computable, use previous vel
                new_vel = self.vel
            # Compute other params    
            new_angVel = newCarPos.angle() - self.carPos.angle()
            new_acc = new_vel - self.vel
            new_angAcc = new_angVel - self.angVel
            # Store new values
            self.vel = new_vel
            self.angVel = new_angVel
            self.acc = new_acc
            self.angAcc = new_angAcc
            self.carPos = newCarPos

class Brain(object):

    def __init__(self, name):
        self.name = name
        # Decision variables
        self.throttle = 1
        self.throttle_bend = 0.8
        self.switchLane = "No"
        self.turboAvailable = False
        self.turboActive = False
        # Dict indexed by car name
        self.carStates = {}
        # Max seen values
        self.seenCrash = False
        self.maxAng = 60
        # Learning
        self.iniLearnFlag = True
        self.velHistory = []
        self.brkHistory = []
        self.tau = 50
        self.vmax = 10
        # Last sent values
        self.last_throttle = self.throttle
        self.last_switchLane = self.switchLane
    
    def gotTurbo(self, data):
        self.turboAvailable = True
    
    def turboStart(self):
        self.turboActive = True
        self.turboAvailable = False
        print "Brain: GOGOGO TURBO!"
        
    def turboEnd(self):
        self.turboActive = False
        print "Brain: TURBO is over :'("
        
    def sent_throttle(self, throttle):
        """
        To be notified when a new throttle is actually sent
        """
        self.last_throttle = throttle
        
    def sent_switchLane(self, switchLane):
        """
        To be notified when a new switchLane is actually sent
        """
        self.last_switchLane = switchLane
           
    def initCarStates(self):
        """
        Initialization of carStates. Needs track to be already learnt.
        To be used at the begining of each race.
        """
        self.carStates = {}
        for car in self.track.cars():
            self.carStates[car['id']['name']] = CarState(car['id']['name'])
        
    def resetRace(self):
        """
        Re-initialization actions to be performed at the begining of each race
        """
        self.throttle = 1
        self.throttle_bend = 0.8
        self.switchLane = "No"
        self.initCarStates()
        self.turboAvailable = False
        self.turboActive = False
        self.last_throttle = self.throttle
        self.last_switchLane = "No"

    def learnTrack(self, data):
        self.track = trck.Track(data)
        self.initCarStates()
    
    def crash(self, data):
        """ 
        Someone crashed: note down angle so as not to crash in the furute
        """
        self.seenCrash = True
        name = data['name']
        self.learnMaxValues(self.carStates[name])
        # Reset velocity, acceleration, etc
        self.carStates[name].reset()
        self.carStates[name].crashed = True
        print("%s crashed" % data['name'])
        
    def spawn(self, data):
        """ 
        Someone spawned
        """
        name = data['name']
        self.carStates[name].crashed = False
        print("%s spawned" % data['name'])
        
    def learnMaxValues(self, carState):
        if self.seenCrash and abs(carState.carPos.angle()) > abs(self.maxAng):
            self.maxAng = abs(carState.carPos.angle())
            print "New maxAng by '", carState.carPos.name(),"': ", self.maxAng
        
    def updateCarStates(self, data, gameTick):
        """
        Updates status for each car in carPositions msg
        """
        for car in data:
            name = car['id']['name']
            pos = carpos.CarPos(name, data)
            # Update car State info
            self.carStates[name].update(pos, self.track, gameTick)
            # Check for new max values
            self.learnMaxValues(self.carStates[name])
            
    def routePlan(self):
        """
        Decide wether a switch message should be sent
        """
        ret_switch = "No"
        
        # Get own state
        state = self.carStates[self.name]
        
        # If we are changing lane
        if state.carPos.startLaneIndex() != state.carPos.endLaneIndex():
            # Last switch command executed, reset
            self.last_switchLane = "No"
            
        # ELSE, Decide wether to switch lane or not, if no previous switch
        # command has been sent.
        elif self.last_switchLane == "No":
        
            # Avoid traffic
            ret_switch = self.switch_avoidTraffic()
            
            # if no traffic ahead, follow optimal route
            if ret_switch == "No":
                ret_switch = self.switch_shortestRoute()
            
        return ret_switch
    
    def switch_shortestRoute(self):
        """
        Returns a 'switch' command for the shortest route
        """
        ret_switch = "No"
        
        # Get own state and pos
        state = self.carStates[self.name]
        pos = state.carPos
        
        nextSwitchIndex = self.track.nextSwitchIndex(
            self.track.nextPieceIndex(pos.pieceIndex())
            )
        goalSwitchIndex = self.track.nextSwitchIndex(
            self.track.nextPieceIndex(nextSwitchIndex)
            )
        
        # Compute lengths of each lane
        distByLane = {}
        for laneIndex in [laneIndex['index'] for laneIndex in self.track.lanes()]:
            distByLane[laneIndex] = \
                self.track.distBetweenPieces(nextSwitchIndex, goalSwitchIndex, laneIndex)
        
        # Compute left and right lane indexes
        rightIndex = self.track.shiftLaneIndex(pos.endLaneIndex(), shiftRight=True)   
        leftIndex = self.track.shiftLaneIndex(pos.endLaneIndex(), shiftRight=False)
        
        # retrieve current route length
        dist = distByLane[pos.endLaneIndex()]
        
        # If 'Right' route is shorter, switch Right
        if rightIndex is not None and distByLane[rightIndex] < dist:
            dist = distByLane[rightIndex]
            ret_switch = 'Right'
            
        # If 'Left' route is shorter, switch Left
        if leftIndex is not None and distByLane[leftIndex] < dist:
            dist = distByLane[leftIndex]
            ret_switch = 'Left'
        
        #print "Route: pos %s lane %s, next_switch %s goalSwitch %s, dist %s --> Switch: %s (rightIndex: %s leftIndex: %s)" \
        #    % (pos.pieceIndex(), pos.endLaneIndex(), nextSwitchIndex, goalSwitchIndex, distByLane, ret_switch, rightIndex, leftIndex)
        
        return ret_switch
        
    def switch_avoidTraffic(self):
        """
        Returns a 'switch' command if there is traffic ahead
        """
        ret_switch = "No"
        
        # Get own state
        state = self.carStates[self.name]
        
        # Easy/stupid checking: someone in front? change lane (should probably be combined with a route analysis)
        #better something like return lane for lane in self.lanes() if lane["index"] == laneIndex]
        for car in self.carStates.items():  
            if car[0] != self.name:     #Check that we're not comparing with owrselves
                hisState = car[1]       #Get the state from the tuple
                #print 'Distance to' + str(car[0]) + ': ' + str(self.track.distBetweenPositions(hisState.carPos,state.carPos))
                
                #CHECK 1: Pieces ahead
                pieceDistance = self.track.piecesInBetween(state.carPos.pieceIndex(),hisState.carPos.pieceIndex())
                if pieceDistance < 4:
                    print state.carPos.pieceIndex(),hisState.carPos.pieceIndex(),pieceDistance
                    print 'Car close!, at ' + repr(pieceDistance) + ' pieces'
                    #CHECK 2: Same line?
                    if hisState.carPos.startLaneIndex() == state.carPos.startLaneIndex():
                        print("He's on same line")
                        #CHECK 3: Close enough? / change for...velocity difference
                        distance = self.track.distBetweenPositions(hisState.carPos,state.carPos)
                        if distance < 80 and distance > 0:
                            print "Switch needed, he's in front, at " + repr(distance)
                            #DECISION: where to change to
                            if self.track.laneLeft(state.carPos):
                                ret_switch = "Left"
                            elif self.track.laneRight(state.carPos):
                                ret_switch = "Right"
                            else:
                                ret_switch = "No"
                    else:
                        ret_switch = "No"
                else:
                    ret_switch = "No"
        
        return ret_switch
    
    def initialLearning(self, gameTick):
        state = self.carStates[self.name]
        throttle = 1
        throttle_learn = 0.5
        
        if gameTick <= 1:
            # Set acceleration
            throttle = throttle_learn
        elif gameTick < 100:
            # Set acceleration
            throttle = throttle_learn
            self.velHistory.append(state.vel)
        elif gameTick == 100:
            # Set break 
            throttle = 0
        elif gameTick < 150:
            # Set break
            throttle = 0
            self.brkHistory.append(state.vel)
        elif gameTick == 150:
            # learn from break deceleration curve
            tau_break=[]
            for v1, v2 in zip(self.brkHistory[::2], self.brkHistory[1::2]):
                if v1 == 0: v1 = 0.0000001
                if v2 == 0: v2 = 0.0000001
                tau = -1.0/(log(v2)-log(v1))
                tau_break.append(tau)
            # Store TAU of BREAK
            self.tau = mean(tau_break)    
            print  "Learnt TAU BREAK: ", self.tau
            
            # learn from acceleration curve
            vmax = []
            for v1, v2 in zip(self.velHistory[::2], self.velHistory[1::2]):
                if v1 == 0: v1 = 0.0000001
                if v2 == 0: v2 = 0.0000001
                beta = exp(-1.0/self.tau)
                vmax.append((beta*v1-v2)/(beta-1)/throttle_learn)
            # Store VMAX
            self.vmax = mean(vmax)   
            print  "Learnt VMAX: ", self.vmax
        else:
            self.iniLearnFlag = False
        
        return throttle
    
    def predictAngle(self, state, numTicks=5):
        
        ret_angle = state.carPos.angle() \
            + state.angVel*numTicks \
            + 0.5*state.angAcc*numTicks*numTicks
            
        #print "predictAngle ", ret_angle
        
        return ret_angle
        
    def velForRadius(self, radius):
        if radius > 110:
            return self.vmax
        elif radius > 90:
            return 8.5
        elif radius > 80:
            return 7
        elif radius > 70:
            return 5
        elif radius > 60:
            return 4.5
        elif radius > 50:
            return 3
        elif radius > 40:
            return 2
        elif radius > 30:
            return 1.5
        elif radius > 20:
            return 1.3
        else:
            return 1
        
    def simpleLimiter(self):
        throttle_max = 1
        safety_factor = 0.90
        ret_throttle = throttle_max
        
        state = self.carStates[self.name]
        pos = state.carPos
        
        # Limit speed in bends
        if self.track.isBend(pos.pieceIndex()):
            
            pred_angle = self.predictAngle(state, 15)
            if abs(pred_angle) > self.maxAng*safety_factor:
                print "Echando el ancla en %s" % state
                ret_throttle = 0.0
            elif abs(pred_angle) < self.maxAng*0.5:
                self.throttle_bend += 0.05
                if self.throttle_bend > 1:
                    self.throttle_bend = 1
                ret_throttle = self.throttle_bend
            else:
                ret_throttle = self.throttle_bend       
        
        # break if aproaching bend
        nextBendIndex = self.track.nextBendIndex(pos.pieceIndex())
        distToBend = self.track.distFromPosToPiece(pos, nextBendIndex)
        targetVel = self.velForRadius(self.track.pieceRad(nextBendIndex, pos.endLaneIndex()))
        #print "pos: ", pos.pieceIndex(), " - ", pos.inPieceDistance(), "nextBend: ", nextBendIndex
        #print "simpleLimiter: vel", state.vel, "Dist ", distToBend, " comp: ", str(throttle_limited*10*safety_factor+distToBend*self.tau)
        
        if state.vel < targetVel*safety_factor+distToBend/self.tau:
            # Distance is enough, keep GAS
            ret_throttle_approach = throttle_max
        elif state.vel < targetVel*safety_factor:
            ret_throttle_approach = targetVel/self.vmax
        else:
            # limit speed
            ret_throttle_approach = 0
            
        if ret_throttle_approach < ret_throttle:
            ret_throttle = ret_throttle_approach
        
        return ret_throttle
        
    def think(self, data, gameTick):
        """
        Main function in Brain:
        Think new throttle and (if applicable) lane should be switched.
        Returns: throttle, switch
        """
        
        # Compute velocities, accelerations, etc
        self.updateCarStates(data, gameTick)
        
        # Initial learning phase
        if self.iniLearnFlag:
            self.throttle = self.initialLearning(gameTick)
        else:
            self.throttle = self.simpleLimiter()
            # Plan route
            self.switchLane = self.routePlan()
        
        # Get own state
        state = self.carStates[self.name]
        
        #if self.track.hasSwitch(state.carPos.pieceIndex()): print "There's a SWITCH now: pieceIndex = ", state.carPos.pieceIndex()
        
        self.turbo = False
        if self.turboAvailable:
            # TODO
            if not self.track.isBend(state.carPos.pieceIndex()) \
                and not self.track.isBend(self.track.nextPieceIndex(state.carPos.pieceIndex())):
                self.turbo = True
        else:
            self.turbo = False
        
        #print "Tick: ", gameTick, state
        #print repr(gameTick)+"\t"+repr(self.throttle)+"\t"+repr(state.vel)+"\t"+repr(state.carPos.angle())+"\t"+repr(self.track.pieceRad(state.carPos.pieceIndex(),0))
        
        #Store data in easy-to-plot format:
        if True:
            with open("test.txt", "a") as myfile:
                myfile.write(repr(gameTick)+
                             "\t"+repr(self.throttle)+
                             "\t"+repr(state.vel)+
                             "\t"+repr(state.acc)+
                             "\t"+repr(state.carPos.angle())+
                             "\t"+repr(state.angVel)+
                             "\t"+repr(state.angAcc)+
                             "\t"+repr(state.carPos.lap())+
                             "\t"+repr(state.carPos.pieceIndex())+
                             "\t"+repr(self.track.pieceRad(state.carPos.pieceIndex(),state.carPos.startLaneIndex()))+
                             "\t"+repr(state.carPos.startLaneIndex())+
                             "\t"+repr(self.track.pieceRad(state.carPos.pieceIndex(),0))+
                             "\t"+repr(self.track.pieceRad(state.carPos.pieceIndex(),1))
                            )
                myfile.write("\n")
                
        return self.throttle, self.switchLane, self.turbo
        

#######################################################
# Ancillary functions
#######################################################

def mean(l):
    return float(sum(l))/len(l) if len(l) > 0 else float('nan')
