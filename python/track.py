import json
import sys
import carpos
import math

class Track(object):

    def __init__(self, data):
        """ Store the gameInit message information """
        self.data = data
        
        # store  pairs (distanceFromCenter, laneIndex), 
        dist_lane = [ \
            (lanes['distanceFromCenter'], lanes['index']) \
            for lanes in self.lanes() ]
            
        # store  pairs (distanceFromCenter, laneIndex), 
        # ordered by distanceFromCenter
        self.dist_lane = sorted(dist_lane)
        
    def pieces(self):
        return self.data['race']['track']['pieces']
        
    def cars(self):
        return self.data['race']['cars']
        
    def lanes(self):
        return self.data['race']['track']['lanes']
    
    def isBend(self, pieceIndex):
        if pieceIndex >= len(self.pieces()):
            pieceIndex = pieceIndex - len(self.pieces())
        return 'radius' in self.pieces()[pieceIndex]
        
    def hasSwitch(self, pieceIndex):
        if pieceIndex >= len(self.pieces()):
            pieceIndex = pieceIndex - len(self.pieces())
        
        try:
            return self.data['race']['track']['pieces'][pieceIndex]['switch']
        except Exception:
            return False
            
    def shiftLaneIndex(self, laneIndex, shiftRight=True):
        """
        Returns the next index to the Right (if shiftRight == True)
        or to the left (shiftRight == False)
        """
        ret = None
        
        # Find laneIndex in the sorted list (sorted by distanceFromCenter)
        # self.dist_lane contains tuples (distanceFromCenter, laneIndex)
        lane = [el for el in self.dist_lane if el[1] == laneIndex]
        indexSorted = self.dist_lane.index(lane[0])
        
        # distanceFromCenter > 0 means to the right
        if shiftRight:
            # check list index bounds
            if indexSorted+1 < len(self.dist_lane):
                ret = indexSorted+1
        else:
            # check list index bounds
            if indexSorted-1 >= 0:
                ret = indexSorted-1
        
        return ret
    
    def laneRight(self, carPos):
        """
        Returns true if there's a lane to the right"
        """
        myLane = carPos.startLaneIndex()
        laneIndices = [lanes['index'] for lanes in self.lanes()] #obtains a list of lane indeces
        
        if myLane+1 in laneIndices:
            print ('Lane to the RIGHT')
            return True
        else:
            return False
    
    def laneLeft(self, carPos):
        """
        Returns true if there's a lane to the left"
        """
        myLane = carPos.startLaneIndex()
        laneIndices = [lanes['index'] for lanes in self.lanes()] #obtains a list of lane indeces
        
        if myLane-1 in laneIndices:
            print ('Lane to the LEFT')
            return True
        else:
            return False
    
    def piecesIndexDist(self, iniPieceIndex, endPieceIndex):
        """
        Returns the index distance between to pieces, 
        taking into account that the indexes are traversed in increasing order.
        """
        # Most usual case
        if endPieceIndex >= iniPieceIndex:
            ret = endPieceIndex - iniPieceIndex
        
        # Rollback: endPieceIndex < iniPieceIndex
        else:
            numPieces = len(self.pieces())
            endPieceIndex += numPieces
            ret = endPieceIndex - iniPieceIndex
            
        return ret
        
    def piecesInBetween(self, iniPieceIndex, endPieceIndex):
        """
        Returns the number pieces in between two given pieces, 
        taking into account that the indexes are traversed in increasing order.
        
        i.e: piecesBetween(0,3) = 2
        
        i.e: if numpieces = 10, piecesBetween(3,0) = 6
        """
        indexDist = self.piecesIndexDist(iniPieceIndex, endPieceIndex)
        
        if indexDist >= 1:
            numPieces = indexDist - 1
        else:
            numPieces = 0
            
        return numPieces
    
    def nextPieceIndex(self, pieceIndex, offset=1):
        """
        Returns the next piece index. This function handles index rollover.
        
        If offset is provided, the index of the 'currentPieceIndex + offset' 
        piece is returned.
        
        i.e: offset=2  --> next next piece
             offset=-1 --> previous piece
        """
        newIndex = pieceIndex + offset;
        # Handle rollover
        if newIndex >= len(self.pieces()):
            newIndex -= len(self.pieces())
        if newIndex < 0:
            newIndex += len(self.pieces())
            
        return newIndex
        
    def pieceLen(self, pieceIndex, startLaneIndex = 0, endLaneIndex = 0):
        if startLaneIndex != endLaneIndex:
            # TODO: can't compute lenght of lane switches!!!
            #raise ErrorComputeLaneSwitchLen(
            #    "TODO: cant compute pieceLen of piece %d with lane switch" \
            #    % pieceIndex
            #    )
            #print "TODO: cant compute pieceLen of piece %d with lane switch" % pieceIndex
            pass
        
        if self.isBend(pieceIndex):
            # Piece is bend
            radius_base = self.pieces()[pieceIndex]['radius']
            angle = self.pieces()[pieceIndex]['angle']
            
            # take into account lane into radius
            for lane in self.lanes():
                if lane['index'] == startLaneIndex:
                    distanceFromCenter = lane['distanceFromCenter']
            
            # Compute effective radius (depends on lane and turn right/left)
            # lane_dist > 0 means to the right
            # angle > 0 means bend to the right
            if angle >= 0:
                radius = radius_base - distanceFromCenter
            else:
                radius = radius_base + distanceFromCenter
            
            # compute len
            pieceLen = math.pi*radius*abs(angle)/180.0
        else:
            # Not a bend
            pieceLen = self.pieces()[pieceIndex]['length']
        
        return pieceLen
    
    def distBetweenPositions(self, iniCarPos, endCarPos):
        """
        Distance between two positions
        """
        dist = 0
        iniPiece = iniCarPos.pieceIndex()
        endPiece = endCarPos.pieceIndex()
        if iniPiece !=0 and endPiece == 0: endPiece += len(self.pieces())
        for pieceID in range(iniPiece, endPiece):
            dist += self.pieceLen(pieceID)
        dist = dist + endCarPos.inPieceDistance() - iniCarPos.inPieceDistance()
        
        return dist
    
    def distBetweenPieces(self, iniPieceIndex, endPieceIndex, laneIndex):
        """
        Returns the total length of the pieces between two given pieces
        """
        dist = 0
        numPieces = self.piecesInBetween(iniPieceIndex, endPieceIndex)
        for offset in range(1,numPieces+1):
            index = self.nextPieceIndex(iniPieceIndex, offset)
            dist += self.pieceLen(index, laneIndex, laneIndex)
     
        return dist
        
    def distFromPosToPiece(self, iniCarPos, endPieceIndex):
        iniPieceIndex = iniCarPos.pieceIndex()
        dist = self.distBetweenPieces(
            iniPieceIndex, 
            endPieceIndex, 
            iniCarPos.endLaneIndex())
        dist += self.pieceLen(
            iniPieceIndex, 
            iniCarPos.startLaneIndex(),
            iniCarPos.endLaneIndex())
        dist -= iniCarPos.inPieceDistance()
        return dist
        
    def nextBendIndex(self, currentPieceIndex):
        for offset in range(len(self.pieces())):
            index = self.nextPieceIndex(currentPieceIndex, offset)
            if self.isBend(index):
                return index
                
    def nextSwitchIndex(self, currentPieceIndex):
        for offset in range(len(self.pieces())):
            index = self.nextPieceIndex(currentPieceIndex, offset)
            if self.hasSwitch(index):
                return index
    
    def lane(self, laneIndex):
        """
        Returns de lane with laneIndex
        """
        return (lane for lane in self.lanes() if lane["index"] == laneIndex).next()
    
    def pieceRad(self,pieceIndex,laneIndex):
        """
        Returns the effective radius of a piece/lane
        """
        if pieceIndex >= len(self.pieces()): #Useless?
            pieceIndex = pieceIndex - len(self.pieces())
        
        if self.isBend(pieceIndex):
            baseRadius = self.pieces()[pieceIndex]['radius']
            angle = self.pieces()[pieceIndex]['angle']
            
            if angle > 0:
                return baseRadius - self.lane(laneIndex)['distanceFromCenter']
            elif angle < 0:
                return baseRadius + self.lane(laneIndex)['distanceFromCenter']
            else:
                return 999
        else:
            return 999
    
class TrackException(Exception):
    def __init__(self, message):
        self.message = message
        
class ErrorComputeLaneSwitchLen(TrackException):
    def __init__(self, message):
        self.message = message
