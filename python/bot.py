import json
import socket
import sys
import brain

class Bot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.gameTick = -1
        self.brain = brain.Brain(name)

    def msg(self, msg_type, data, sendTick = False):
        if sendTick and self.gameTick >= 0:
            self.send(json.dumps(
                {"msgType": msg_type, "data": data, "gameTick": self.gameTick}
                ))
        else:
            self.send(json.dumps(
                {"msgType": msg_type, "data": data}
                ))

    def send(self, msg):
        self.socket.sendall(msg + "\n")
        #print "Sent: ", msg

    def join(self):
        
        if True:
            # Main join
            return self.msg("join", {"name": self.name,
                                    "key": self.key})
        
        else:
            # Alternative join
            return self.msg("joinRace", {
                "botId": {
                  "name": self.name,
                  "key": self.key
                },
                "trackName": "usa",
                "password": "rea",
                "carCount": 1
              })

    def throttle(self, throttle):
        self.msg("throttle", throttle, sendTick = True)
        self.brain.sent_throttle(throttle)
        
    def switchLane(self, switchLane, sendTick = True):
        print "SWITCH SENT to the ", switchLane, " , Tick:", self.gameTick
        self.msg("switchLane", switchLane)
        self.brain.sent_switchLane(switchLane)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()
        
    def turbo(self):
        self.msg("turbo", "Turbo activated!")
        print "Turbo activated!"
        self.brain.turboStart()
        
    def on_turbo_available(self, data):
        #Read the info about the turbo and parse it to the brain
        self.brain.gotTurbo(data)
        print "Received Turbo: ", data
    
    def on_turbo_start(self,data):
        self.brain.turboStart()
    
    def on_turbo_end(self, data):
        self.brain.turboEnd()

    def on_join(self, data):
        print("Joined")

    def on_game_start(self, data):
        print("Race started")
        self.msg("throttle", 1)
        self.brain.sent_throttle(1)

    def on_car_positions(self, data):       
        #print("Got car positions for tick %d" % self.gameTick)
        
        if self.gameTick >= 0:
            # Think only if tick is valid (race is ongoing)
            throttle, switchLane, turbo = self.brain.think(data, self.gameTick)
            # Send response
            #print 'Change to the' + str(switchLane) + ' received'
            if switchLane == "Right" or switchLane == "Left":
                self.switchLane(switchLane)
            elif turbo:
                self.turbo()
            else:
                self.throttle(throttle)
        else:
            # Just update car positions (race not ongoing)
            self.brain.updateCarStates(data, self.gameTick)
            # No response needed

    def on_crash(self, data):
        self.brain.crash(data)

    def on_game_end(self, data):
        print("Race ended")
        self.brain.resetRace()
        
    def on_game_init(self, data):
        self.brain.learnTrack(data)
        print "Track: ", data
        self.ping()

    def on_spawn(self, data):
        self.brain.spawn(data)
        
    def on_error(self, data):
        print("Error: {0}".format(data))

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
            'gameInit': self.on_game_init,
            'turboAvailable': self.on_turbo_available,
            'turboEnd': self.on_turbo_end,
            'turboStart': self.on_turbo_start,
            'spawn': self.on_spawn,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            
            # Store gametick if provided
            if 'gameTick' in msg:
                self.gameTick = msg['gameTick']
            else:
                # invalidate gametick
                self.gameTick = -1
            
            # Perform actions
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))

            line = socket_file.readline()
