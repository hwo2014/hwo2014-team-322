import json
import sys

class CarPos(object):
    
    def __init__(self, name, data=None):
        """ 
        Create CarPos object from one car in carpositions msg.
        name is the name of the car to be extracted from data.
        Default position is assumed if no data is provided  
        """
        
        if data == None:
            # DEFAULT POS
            self.data = {
                    u'id': {u'name': name, u'color': u'red'}, 
                    u'angle': 0.0,
                    u'piecePosition': {
                        u'pieceIndex': 0,
                        u'inPieceDistance': 0.0,
                        u'lane': {
                            u'startLaneIndex': 0,
                            u'endLaneIndex': 0
                        },
                        u'lap': 0
                    }
                }
        else:
            # READ POS FROM DATA
            for pos in data:
                if pos['id']['name'] == name:
                    self.data = pos
                    
    def __str__(self):
        return str(self.data)
        
    def angle(self):
        return self.data['angle']
                        
    def pieceIndex(self):
        return self.data['piecePosition']['pieceIndex']
        
    def inPieceDistance(self):
        return self.data['piecePosition']['inPieceDistance']
        
    def lap(self):
        return self.data['piecePosition']['lap']
        
    def name(self):
        return self.data['id']['name']
        
    def startLaneIndex(self):
        return int(self.data['piecePosition']['lane']['startLaneIndex'])
        
    def endLaneIndex(self):
        return int(self.data['piecePosition']['lane']['endLaneIndex'])
