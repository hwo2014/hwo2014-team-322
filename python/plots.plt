set term png
set output "plots/ang_rad.png"
set ytics nomirror
set y2tics
plot 'test.txt' u 1:5 title "Angle" axes x1y1, 'test.txt' u 1:10 title "Radius" axes x1y2
set output "plots/vel_ang.png"
plot 'test.txt' u 1:5 title "Angle" axes x1y1, 'test.txt' u 1:3 title "Velocity" axes x1y2
set output "plots/angvel_ang.png"
plot 'test.txt' u 1:6 title "AngVel" axes x1y1, 'test.txt' u 1:5 title "Angle" axes x1y2

set palette model RGB defined ( 0 'red', 1 'green' )
set output "plots/exp.png"
plot "test.txt" u 1:5:( $10 == 999 ? 0 : 1 ) with points palette title "Angle"

set output "plots/radius.png"
plot "test.txt" u 1:12 title "lane 0", 'test.txt' u 1:13 title "lane 1"
